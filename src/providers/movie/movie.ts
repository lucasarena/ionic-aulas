import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MovieProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MovieProvider {
  private baseApiPath:string = "https://api.themoviedb.org/3";
  private apiKey:string = "659d2904005d90dcbb25a41cfe414b09";

  constructor(public http: HttpClient) {
  }

  getLastMovies() {
    return this.http.get(this.baseApiPath + '/movie/550?api_key=' + this.apiKey);
  }
}
