import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MovieProvider } from '../../providers/movie/movie';
import { Provider } from '@angular/compiler/src/core';

/**
 * Generated class for the FeedPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
  providers: [
    MovieProvider
  ]
})
export class FeedPage {
  public obj_feed = {
    title: 'Lucas Arena',
    date: '11/05/1998',
    desctiption: 'Esse e meu primeiro app',
    qtd_likes: 12,
    qtd_comments: 30,
    time_comment: '11h ago'
  };
  public usuario:string = "Lucas Arena";

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private movieProvider: MovieProvider) {
  }

  public somaDoisNumeros():void {
  }

  ionViewDidLoad() {
    this.movieProvider.getLastMovies().subscribe(
      data => {
        console.log(data);
      }, error => {
        console.log(error);
      }
    );
  }

}
